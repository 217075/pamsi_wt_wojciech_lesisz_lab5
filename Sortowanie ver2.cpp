
#define _USE_MATH_DEFINES
#include "stdafx.h"
#include <cstdio> 
#include <stdlib.h>     
#include <string>
#include <iostream>
#include <ctime>
#include <cmath>
#define M_LN2      0.693147180559945309417
using namespace std;



//****		Algorytmy sortujace:		****//

//* Introspektywne *//
template <typename T>
void swap(T tab[], T s, T k)
{
	T x;
	x = tab[s];
	tab[s] = tab[k];
	tab[k] = x;

}
template <typename T>
void intro(T tab[], T start, T koniec, int M)
{
	if (M <= 0) {
		return kopcu(tab, start, koniec+1);	//kopcowanie
	}
	else {
		T srodek = (tab[(start + koniec) / 2]);
		T i = start, j = koniec, k = start, l = 0, x;
		x = tab[(start + koniec) / 2];
		tab[(start + koniec) / 2] = tab[koniec];
		tab[koniec] = x;
		while (i <= j) {
			if (tab[i] < srodek) {
				x = tab[k];
				tab[k] = tab[i];

				tab[i] = x;
				k++;
				i++;
			}
			else { i++; }
		}
		x = tab[k];
		tab[k] = srodek;
		tab[koniec] = x;
		k++;
		l = k;
		i = start;
		while (i <= j) {
			if (tab[i] > srodek) {
				tab[l] = tab[i];
				l++;
				i++;
			}
			else { i++; }
		}
		l = l - k;

		if (start < j && k - start >= 1)
		{
			intro(tab, start, k - 1, M - 1);
		}

		if (k - 1 <= koniec && l > 0) {
			intro(tab, k, koniec, M - 1);
		}

	}
}



template <typename T>
void kopcu(T tab[], T start, T size)
{
	T *tt = new T[(size - start)];
	T i;
	T temp;
	for (i = 0; i < size-start; ++i)
	{
		tt[i] = tab[start+i];
	}

	T s = 0, y = i;
	for (i = (y - 1) / 2; i >= s; i--) {
		temp = i;
		T save = tt[i];
		while (temp <= (y - 1) / 2) {

			T k = 2 * temp;
			while (k < (y - 1) && tt[k] < tt[k + 1])
				++k;
			if (save >= tt[k]) {
				break;
			}
			tt[temp] = tt[k];
			temp = k;
		}
		tt[temp] = save;
	}
		for (i = y - 1; i>s; i--) {
				T x;
				x = tt[i];
				tt[i] = tt[s];
				tt[s] = x;
			
				temp = s;
			
			T temp2=i-1;
			T save = tt[temp];
			while (temp <= temp2 / 2) {
				T k = 2 * temp;
				while (k < temp2 && tt[k] <tt[k + 1])
					++k;
				if (save >= tt[k])
					break;
				tt[temp] = tt[k];
				temp= k;
			}
			tt[temp] = save;
		}
		for (i = 0; i < size-start; ++i)
		{
			tab[start+i] = tt[i];
		}


}
//*		Quicksort		*//
template <typename T>
void quick2(T tab[], T start, T koniec)
{
	T srodek = (tab[(start + koniec) / 2]);
	T i = start, j = koniec, k = start, l = 0, x;
	x = tab[(start + koniec) / 2];
	tab[(start + koniec) / 2] = tab[koniec];
	tab[koniec] = x;
	while (i <= j) {
		if (tab[i] < srodek) {
			x = tab[k];
			tab[k] = tab[i];

			tab[i] = x;
			k++;
			i++;
		}
		else { i++; }
	}
	x = tab[k];
	tab[k] = srodek;
	tab[koniec] = x;
	k++;
	l = k;
	i = start;
	while (i <= j) {
		if (tab[i] > srodek) {
			tab[l] = tab[i];
			l++;
			i++;
		}
		else { i++; }
	}
	l = l - k;

	if (start < j && k - start >= 1)
	{
		quick2(tab, start, k - 1);
	}

	if (k - 1 <= koniec && l > 0) {
		quick2(tab, k, koniec);
	}


}

//*	Przez scalanie (Mergesort)	*//
template <typename T>
void mergesort(T tab[], T start, T srodek, T koniec) 
{
	T *temp = new T[(koniec - start)];
	T i = start, j = srodek + 1, k = 0;
	while (i <= srodek&&j <= koniec) {
		if (tab[j] < tab[i]) {
			temp[k] = tab[j];
			j++;
		}
		else {
			temp[k] = tab[i];
			i++;
		}
		k++;
	}
	if (i <= srodek) {
		while (i <= srodek)
		{
			temp[k] = tab[i];
			i++;
			k++;
		}
	}
	else {
		{
			while (j <= koniec)
			{
				temp[k] = tab[j];
				j++;
				k++;

			}
		}
	}

	for (i = 0; i <= koniec - start;i++)
		tab[start + i] = temp[i];
}
template <typename T>
void merge(T tab[], T start, T koniec) // Rozdziela tablice na dwie, wywoluje sortowanie
{
	T srodek;
	if (start != koniec)
	{
		srodek = ((start + koniec) / 2);
		merge(tab, start, srodek);
		merge(tab, srodek + 1, koniec);
		mergesort(tab, start, srodek, koniec);
	}
}



//*******************************************************************************************************************************//
//					Do testowania					*****************************************************************************\\
//*******************************************************************************************************************************//

double obliczSekundy(clock_t czas)
{
	return static_cast <double>(czas) / CLOCKS_PER_SEC;
}

template <typename T>
void wyswietl(T tablica[], T size) {
	cout << "Posortowane: \n";
	for (T i = 0; i < size; ++i)
	{
		cout << i << endl;
		cout << tablica[i] << endl;
		if (tablica[i] > tablica[i + 1]){
		cout << "!";
}
	}
}
	
template <typename T>
void zmienkolejnosc(T tab[],T size) {
	T j = size;
	{
		for (T i = 0; i>j; i++)
		{
			tab[i] = tab[j];
			j--;

		}

	}
}
template <typename T>
bool test(T tab[], T size)
{
	for (T i = 0; i+1 < size; i++)
	{
		if (tab[i] > tab[i + 1]) {
			return 0;
		}
	}
	return 1;

}
template <typename T>
void losuj(T tab[], T size) {
	for (T i = 0; i < size; i++)
	{

		tab[i] = rand();

	}
}
template <typename T>
void losujprocent(T tab[], T size, double procent) {
	T temp = procent / 1000 * size;
	T i = 0;
	for (i; i < temp; i++)
	{

		tab[i] = i;

	}
	for (i; i < size; i++)
	{

		tab[i] = rand()+ temp;
	

	}
}


int main()
{
	long size;
	double x, y, z = 0; //Sluza do pomiaru czasu
	long ile =100;  //Ile tablic posortowac?
	size = 50000;//Wielkosc tablicy
	srand(time(NULL));
	long *tablica = new long [size];
	long zero = 0;
	int corobic=2;
	int co = 7;
	
	//******* Interfejs ********//
	
	cout << "Wprowadz wielkosc tablic:  ";
	cin >> size;
	cout << "\n 1. Mergesort \n 2.Quicksort \n 3.Kopiec \n 4.Introsort\n";
	cout << " Wprowadz od 1-4\n       ";
	cin >> corobic;
	cout << "\n\n  1. 0%    2. 25%    3. 50%    4. 75%    5. 99%    6. 99.7%    7.Odwrotnie\n       ";
	cin >> co;
	

	if (co == 1) {
		//0%
		for (long i = 0; i < ile; i++) {
			losuj(tablica, size);
			x = obliczSekundy(clock());
			if (corobic == 1) {

				merge(tablica, zero, size - 1);
			}
			if (corobic == 2) {
				quick2(tablica, zero, size - 1);
			}
			if (corobic == 3) {
				kopcu(tablica, zero, size);
			}
			if (corobic == 4) {
				intro(tablica, zero, size - 1, floor(2 * log(size) / M_LN2));
			}
			y = obliczSekundy(clock());
			z = z + y - x;
			if (test(tablica, size) != 1)
				cout << "BLAD";
		}

		cout << "Czas trawania sortowania: " << z << endl;
		z = 0;
		if (test(tablica, size) == 1)
			cout << "Posortowane dobrze";
		else cout << "Blad!";
	}
	if (co == 2) {
		//25%
		for (long i = 0; i < ile; i++) {
			losujprocent(tablica, size, 25);
			x = obliczSekundy(clock());
			if (corobic == 1) {
				merge(tablica, zero, size - 1);
			}
			if (corobic == 2) {
				quick2(tablica, zero, size - 1);
			}
			if (corobic == 3) {
				kopcu(tablica, zero, size);
			}
			if (corobic == 4) {
				intro(tablica, zero, size - 1, floor(2 * log(size) / M_LN2));
			}
			y = obliczSekundy(clock());
			z = z + y - x;
		}
		cout << "\nCzas trawania sortowania: " << z << endl;
		z = 0;
		if (test(tablica, size) == 1)
			cout << "\nPosortowane dobrze";
		else cout << "Blad!";
	}
	if (co == 3) {
		//50%
		for (long i = 0; i < ile; i++) {
			losujprocent(tablica, size, 500);
			x = obliczSekundy(clock());
			if (corobic == 1) {
				merge(tablica, zero, size - 1);
			}
			if (corobic == 2) {
				quick2(tablica, zero, size - 1);
			}
			if (corobic == 3) {
				kopcu(tablica, zero, size);
			}
			if (corobic == 4) {
				intro(tablica, zero, size - 1, floor(2 * log(size) / M_LN2));
			}
			y = obliczSekundy(clock());
			z = z + y - x;
		}
		cout << "\nCzas trawania sortowania: " << z << endl;
		z = 0;
		if (test(tablica, size) == 1)
			cout << "\nPosortowane dobrze";
		else cout << "Blad!\n";
	}
	if (co == 4) {
		//75%
		for (long i = 0; i < ile; i++) {
			losujprocent(tablica, size, 750);
			x = obliczSekundy(clock());
			if (corobic == 1) {
				merge(tablica, zero, size - 1);
			}
			if (corobic == 2) {
				quick2(tablica, zero, size - 1);
			}
			if (corobic == 3) {
				kopcu(tablica, zero, size);
			}
			if (corobic == 4) {
				intro(tablica, zero, size - 1, floor(2 * log(size) / M_LN2));
			}
			y = obliczSekundy(clock());
			z = z + y - x;
		}
		cout << "\nCzas trawania sortowania: " << z << endl;
		z = 0;
		if (test(tablica, size) == 1)
			cout << "\nPosortowane dobrze";
		else cout << "Blad!";

	}
	 //99%
	if (co == 5) {
		for (long i = 0; i < ile; i++) {
			losujprocent(tablica, size, 990);
			x = obliczSekundy(clock());
			if (corobic == 1) {
				merge(tablica, zero, size - 1);
			}
			if (corobic == 2) {
				quick2(tablica, zero, size - 1);
			}
			if (corobic == 3) {
				kopcu(tablica, zero, size);
			}
			if (corobic == 4) {
				intro(tablica, zero, size - 1, floor(2 * log(size) / M_LN2));
			}
			y = obliczSekundy(clock());
			z = z + y - x;
		}
		cout << "\nCzas trawania sortowania: " << z << endl;
		z = 0;
		if (test(tablica, size) == 1)
			cout << "\nPosortowane dobrze";
		else cout << "Blad!";
	}
	if (co == 6) {
		//99.7%
		for (long i = 0; i < ile; i++) {
			losujprocent(tablica, size, 997);
			x = obliczSekundy(clock());
			if (corobic == 1) {
				merge(tablica, zero, size - 1);
			}
			if (corobic == 2) {
				quick2(tablica, zero, size - 1);
			}
			if (corobic == 3) {
				kopcu(tablica, zero, size);
			}
			if (corobic == 4) {
				intro(tablica, zero, size - 1, floor(2 * log(size) / M_LN2));
			}
			y = obliczSekundy(clock());
			z = z + y - x;
		}
		cout << "\nCzas trawania sortowania: " << z << endl;
		z = 0;
		if (test(tablica, size) == 1)
			cout << "\nPosortowane dobrze";
		else cout << "Blad!";
	}
	if (co == 7) {
		//odwrotna kolejnosc
		for (long i = 0; i < ile; i++) {
			losuj(tablica, size);
			quick2(tablica, zero, size - 1);
			zmienkolejnosc(tablica, size);
			x = obliczSekundy(clock());
			if (corobic == 1) {
				merge(tablica, zero, size - 1);
			}
			if (corobic == 2) {
				quick2(tablica, zero, size - 1);
			}
			if (corobic == 3) {
				kopcu(tablica, zero, size);
			}
			if (corobic == 4) {
				intro(tablica, zero, size - 1, floor(2 * log(size) / M_LN2));
			}
			y = obliczSekundy(clock());
			z = z + y - x;
		} 
		cout << "\nCzas trawania sortowania: " << z << endl;
		z = 0;
		if (test(tablica, size) == 1)
			cout << "\nPosortowane dobrze";
		else cout << "Blad!";
	}
	cout << "\nkoniec";
	system("pause");
    return 0;
}

