// ConsoleApplication15.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdio> 
#include <stdlib.h>     
#include <string>
#include <iostream>
#include <ctime>
using namespace std;

//****		Algorytmy sortujace:		****//




//*	Szybkie (Quicksort)	*//


void quick2(int tab[], int start, int koniec)
{
	int *temp = new int[(koniec-start)];
	int *temp2 = new int[(koniec-start)];
	int srodek = (tab[start]);
	int i = start, j = koniec, k = 0, l = 0;
	i++;
	while (i <= j) {
		if (tab[i] < srodek) {
			temp[k] = tab[i];
			k++;
			i++;
		}
		else {
			temp2[l] = tab[i];
			i++; l++;
		}
	}

	temp[k] = tab[start];
	k++;
	i++;


	for (int t = start;t < (start + k);++t)
	{
		tab[t] = temp[t - start];
	}

	for (int t = start + k;t <= koniec;++t)
	{
		tab[t] = temp2[t - (start + k)];
	}


	if (start < j && k >= 1)
	{
		quick2(tab, start, start + (k - 1));
	}


	if (start + k - 1 <= koniec && l > 0) {
		quick2(tab, start + k, koniec);
	}

}

//*	Przez scalanie (Mergesort)	*//

void mergesort(int tab[], int start, int srodek, int koniec) //sortowanie
{
	int *temp = new int[(koniec - start)];
	int i = start, j = srodek + 1, k = 0;
	while (i <= srodek&&j <= koniec) {
		if (tab[j] < tab[i]) {
			temp[k] = tab[j];
			j++;
		}
		else {
			temp[k] = tab[i];
			i++;
		}
		k++;
	}
	if (i <= srodek) {
		while (i <= srodek)
		{
			temp[k] = tab[i];
			i++;
			k++;
		}
	}
	else {
		{
			while (j <= koniec)
			{
				temp[k] = tab[j];
				j++;
				k++;

			}
		}
	}

	for (i = 0; i <= koniec - start;i++)
		tab[start + i] = temp[i];

}
void merge(int tab[], int start, int koniec) // Rozdziela tablice na dwie, wywoluje sortowanie
{
	int srodek;
	if (start != koniec)
	{
		srodek = ((start + koniec) / 2);
		merge(tab, start, srodek);
		merge(tab, srodek+1, koniec);
		mergesort(tab, start, srodek, koniec);
	}
}



//*******************************************************************************************************************************//
//					Do testowania					*****************************************************************************\\
//*******************************************************************************************************************************//

double obliczSekundy(clock_t czas)
{
	return static_cast < double >(czas) / CLOCKS_PER_SEC;
}


void zmienkolejnosc(int tab[],int size) {
	int j = size;
	{
		for (int i = 0; i>j; i++)
		{
			tab[i] = tab[j];
			j--;

		}

	}
}

bool test(int tab[], int size)
{
	for (int i = 0; i+1 < size; i++)
	{
		if (tab[i] > tab[i + 1]) {
			return 0;
		}
	}
	return 1;

}

void losuj(int tab[], int size) {
	for (int i = 0; i < size; i++)
	{

		tab[i] = rand();
		
	}
}

void losujprocent(int tab[], int size, float procent) {
	int temp = procent / 100 * size;
	int i = 0;
	for (i=0; i < temp; i++)
	{

		tab[i] = temp;// % 100;

	}
	for (i = i; i < size; i++)
	{

		tab[i] = rand()+temp;

	}
}

void wyswietl(int tablica[],int size) {
	cout << "Posortowane: \n";
	for (int i = 0; i < size; i++)
	{

		cout << tablica[i] << endl;
	}
}

int main()
{
	int size;
	double x, y, z = 0;
	size = 1000000;//Wielkosc tablicy
	srand(time(NULL));
	int *tablica = new int[size]; //0%
	losuj(tablica, size);
	x= obliczSekundy(clock());
	merge(tablica, 0, size - 1);
//	quick2(tablica, 0, size-1);
	y = obliczSekundy(clock());
	//wyswietl(tablica, size);
	cout << "Czas trawania sortowania: " << y - x<<endl;
	if (test(tablica, size) == 1)
			cout << "Posortowane dobrze";
	else cout << "Blad!";
	delete[] tablica;

	int *tablica2 = new int[size]; //25%
	losujprocent(tablica2, size, 25);
	x = obliczSekundy(clock());
	merge(tablica2, 0, size - 1);
//	quick2(tablica2, 0, size - 1);
	y = obliczSekundy(clock());
	cout << "\nCzas trawania sortowania: " << y - x << endl;
	if (test(tablica2, size) == 1)
		cout << "\nPosortowane dobrze";
	else cout << "Blad!";

	//odwrotna kolejnosc
	zmienkolejnosc(tablica2, size);
	x = obliczSekundy(clock());
	merge(tablica2, 0, size - 1);
//	quick(tablica2, 0, size - 1);
	y = obliczSekundy(clock());
	cout << "\nCzas trawania sortowania: " << y - x << endl;
	if (test(tablica2, size) == 1)
		cout << "\nPosortowane dobrze";
	else cout << "Blad!";
	delete[] tablica2;
	int *tablica4 = new int[size]; //50%
	losujprocent(tablica4, size, 50);
	x = obliczSekundy(clock());
	merge(tablica4, 0, size - 1);
//	quick(tablica4, 0, size - 1);
	y = obliczSekundy(clock());
	cout << "\nCzas trawania sortowania: " << y - x << endl;
	if (test(tablica4, size) == 1)
		cout << "\nPosortowane dobrze";
	else cout << "Blad!";
	delete[] tablica4;
	int *tablica5 = new int[size]; //75%
	losujprocent(tablica5, size,75);
	x = obliczSekundy(clock());
    merge(tablica5, 0, size - 1);
//	quick(tablica5, 0, size - 1);
	y = obliczSekundy(clock());
	cout << "\nCzas trawania sortowania: " << y - x << endl;
	if (test(tablica5, size) == 1)
		cout << "\nPosortowane dobrze";
	else cout << "Blad!";
	delete[] tablica5;
	int *tablica6 = new int[size]; //99%
	losujprocent(tablica6, size, 99);
	x = obliczSekundy(clock());
	merge(tablica6, 0, size - 1);
//	quick(tablica6, 0, size - 1);
	y = obliczSekundy(clock());
	cout << "\nCzas trawania sortowania: " << y - x << endl;

	if (test(tablica6, size) == 1)
		cout << "\nPosortowane dobrze";
	else cout << "Blad!";
	delete[] tablica6;
	int *tablica7 = new int[size]; //99.7%
	losujprocent(tablica7, size, 99.7);
	x = obliczSekundy(clock());
	merge(tablica7, 0, size - 1);
//	quick(tablica7, 0, size - 1);
	y = obliczSekundy(clock());
	cout << "\nCzas trawania sortowania: " << y - x << endl;
	if (test(tablica7, size) == 1)
		cout << "\nPosortowane dobrze";
	else cout << "Blad!";
	delete[] tablica7;
	cout << "\nkoniec";

	cin >> size;
    return 0;



}

